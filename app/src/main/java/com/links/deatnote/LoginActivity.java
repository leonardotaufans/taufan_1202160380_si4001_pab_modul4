package com.links.deatnote;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    /* Login Activity dijadikan sebagai Main/Launcher */
    private static final int REQUEST_READ_CONTACTS = 0;
    CarouselView carouselView;
    int[] images = {R.drawable.lokal_1, R.drawable.lokal_2, R.drawable.lokal_3, R.drawable.lokal_4};
    private static final String TAG = "LoginActivity";
    private FirebaseAuth mAuth;
    private EditText mEmailView;
    private EditText mPasswordView;
    private Button btnMasuk;
    private TextView btnDaftar;
    private View mProgressView;
    private View mLoginFormView;
    private DatabaseReference databaseReference;
    private CoordinatorLayout coordinatorLayout;

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(images[position]);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Log.d(TAG, "You are logged in!");
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Set up CarouselView by Sayyam (github.com/sayyam/carouselview)
        carouselView = (CarouselView) findViewById(R.id.carousel);
        carouselView.setPageCount(images.length);
        carouselView.setImageListener(imageListener);
        // Set up the login form.
        mAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        initializeUI();
        btnMasuk.setOnClickListener(this);
        btnDaftar.setOnClickListener(this);
    }

    void initializeUI() {
        //Inisialisasi layout
        coordinatorLayout = findViewById(R.id.coord_login);
        mProgressView = findViewById(R.id.login_progress);
        mEmailView = (EditText) findViewById(R.id.login_email);
        mPasswordView = findViewById(R.id.login_password);
        btnMasuk = (Button) findViewById(R.id.btn_login);
        btnDaftar = findViewById(R.id.btn_daftar);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "todo: Tell Android Devs that we TOUCH the screen, not click it.");
        switch (v.getId()) {
            case R.id.login_email:
                login();
                break;
            case R.id.btn_daftar:
                daftar();
        }
    }

    private void daftar() {
        //Intent untuk ke RegisterActivity
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    private void login() {
        //Ketika tombol login ditekan
        Log.d(TAG, "LOGIN! -- START");
        mProgressView.setVisibility(View.VISIBLE);

        //Memeriksa apakah email dan password kosong
        if (TextUtils.isEmpty(mEmailView.getText())) {
            Snackbar.make(coordinatorLayout, "Email diperlukan!", Snackbar.LENGTH_LONG)
                    .show();
            mEmailView.setError("Email diperlukan!");
            return;
        }

        if (TextUtils.isEmpty((mPasswordView.getText()))) {
            Snackbar.make(coordinatorLayout, "Password diperlukan!", Snackbar.LENGTH_LONG)
                    .show();
            mPasswordView.setError("Password diperlukan!");
            return;
        }

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        //Signin dengan FirebaseAuth
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "login(): onComplete! " + task.isSuccessful());
                        if (task.isSuccessful()) {
                            Snackbar.make(coordinatorLayout, "Login berhasil!", Snackbar.LENGTH_LONG)
                                    .show();
                            mProgressView.setVisibility(View.GONE);

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }

                        else {
                            Snackbar.make(coordinatorLayout, "Login gagal!", Snackbar.LENGTH_LONG)
                                    .show();
                            mProgressView.setVisibility(View.GONE);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Snackbar.make(coordinatorLayout, "Error!"+e.getLocalizedMessage(), Snackbar.LENGTH_LONG)
                        .show();
            }
        });
    }
}