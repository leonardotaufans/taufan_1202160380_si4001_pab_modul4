package com.links.deatnote;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class NewMenuActivity extends AppCompatActivity {

    private static final String TAG = "NewMenuActivity";
    private DatabaseReference mDatabase;
    private StorageReference mStorageReference;
    private EditText nama, harga, desc;
    private ImageView imageView;
    private Button btn_simpan;
    private MenuAdapter mAdapter;
    private ProgressBar progressBar;
    private Uri mImageUri, downloadUri;
    private static final int PICK_IMAGE_REQ_CODE = 1337;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_menu);
        initializeUI();
        //Initialize Firebase Storage dan Firebase Database
        mStorageReference = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //ImageView diberi onClick, mengganti gambar langsung dengan menyentuh ImageView
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        //Tombol simpan untuk...(drumroll)...menyimpan.
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Verifikasi apakah kolom nama, harga, dan deskripsi kosong.
                if (TextUtils.isEmpty(nama.getText().toString())
                        || TextUtils.isEmpty(harga.getText().toString())
                        || TextUtils.isEmpty(desc.getText().toString())) {
                    Toast.makeText(NewMenuActivity.this, getString(R.string.toast_error_kolom_kosong), Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                //Karena upload gambar terlalu panjang dan supaya tidak berantakan, mereka dipisah dari inner onClick
                upload();
            }
        });
    }

    private String getFileExtension(Uri uri) {
        //Digunakan untuk menangkap...file extension.
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void upload() {
        //Ya, ada progress bar di bagian bawah layar.
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
        //Gambar wajib dimasukkan.
        if (mImageUri != null) {
            final StorageReference fileReference = mStorageReference.child(String.valueOf(System.currentTimeMillis())+"."+getFileExtension(mImageUri));
            final UploadTask uploadTask = fileReference.putFile(mImageUri);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        String taskResult = task.getResult().toString();
                        String getNama = nama.getText().toString();
                        String getDesc = desc.getText().toString();
                        String getHarga = harga.getText().toString();
                        Map<String, Object> taskMap = new HashMap<>();
                        taskMap.put("nama", getNama);
                        taskMap.put("desc", getDesc);
                        taskMap.put("harga", getHarga);
                        taskMap.put("image", taskResult);
                        //Agak bertele-tele, tapi "tumbuh tuh ke atas, gak ke samping." ,-,
                        mDatabase.child("menu").child(getNama).updateChildren(taskMap);
                        progressBar.setVisibility(View.GONE);
                        finish();
                    }
                }
            });

        } else {
            Toast.makeText(this, getString(R.string.toast_error_no_pic_selected), Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void selectImage() {
        //Image Selector
        Log.d(TAG, "Uploading waifu or bias's picture is STRICTLY PROHIBITED");
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //onActivityResult untuk Image Selector
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQ_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Log.d(TAG, "todo: Check whether user uploaded picture of food or picture of their waifu");
            mImageUri = data.getData();
            Picasso.with(this).load(mImageUri).into(imageView);
        }
    }

    private void initializeUI() {
        //Untuk inisialisasi layout
        nama = findViewById(R.id.editText_nama_makanan);
        harga = findViewById(R.id.editText_harga_makanan);
        desc = findViewById(R.id.editText_deskripsi_makanan);
        imageView = findViewById(R.id.image_makanan);
        btn_simpan = findViewById(R.id.btn_simpan_menu);
        progressBar = findViewById(R.id.progress_new_menu);
    }
}

