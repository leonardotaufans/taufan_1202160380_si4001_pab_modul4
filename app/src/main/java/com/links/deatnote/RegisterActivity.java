package com.links.deatnote;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.auth.data.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";
    private EditText mEmail, mPassword1, mPassword2, mNama;
    private Button registBtn;
    private TextView loginBtn;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    private CarouselView carouselView;
    private DatabaseReference mDatabase;

    int[] images = {R.drawable.lokal_1, R.drawable.lokal_2, R.drawable.lokal_3, R.drawable.lokal_4};
    private CoordinatorLayout coord;

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(images[position]);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        Log.d(TAG, "Why do regret only come later? Cause if it's early, it would be called 'Registration'.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //Using CarouselView by Sayyam (github.com/sayyam/carouselview)
        carouselView = (CarouselView) findViewById(R.id.carousel_register);
        carouselView.setPageCount(images.length);
        carouselView.setImageListener(imageListener);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        initializeUI();

        registBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login() {
        Log.d(TAG, "Transferring to LoginActivity");
        Intent login = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(login);
    }

    private void register() {
        Log.d(TAG, "Oh, you're signing in? Sign me up...(?)");
        progressBar.setVisibility(View.VISIBLE);

        final String nama, email, pass1, pass2;
        nama = mNama.getText().toString();
        email = mEmail.getText().toString();
        pass1 = mPassword1.getText().toString();
        pass2 = mPassword2.getText().toString();

        //Bagian ini hanya digunakan untuk memeriksa jika password yang diinput sesuai
        if (!TextUtils.equals(pass1, pass2)) {
            Snackbar.make(coord, "Password tidak sama!", Snackbar.LENGTH_LONG)
                    .show();
            return;
        }

        if (TextUtils.isEmpty(nama)) {
            Snackbar.make(coord, "Nama kosong!", Snackbar.LENGTH_LONG)
                    .show();
            return;
        }

        if (TextUtils.isEmpty(email)) {
            Snackbar.make(coord, "Email kosong!", Snackbar.LENGTH_LONG)
                    .show();
            return;
        }

        if (TextUtils.isEmpty(pass1)) {
            Snackbar.make(coord, "Password kosong!", Snackbar.LENGTH_LONG)
                    .show();
            return;
        }

        if (TextUtils.isEmpty(pass2)) {
            Snackbar.make(coord, "Masukkan kembali password!", Snackbar.LENGTH_LONG)
                    .show();
            return;
        }

        Log.d(TAG, "Signing up");
        mAuth.createUserWithEmailAndPassword(email, pass1)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Snackbar.make(coord, "Registrasi berhasil!", Snackbar.LENGTH_LONG);
                            progressBar.setVisibility(View.GONE);

                            afterRegister(mAuth.getUid(), nama, email);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Snackbar.make(coord, "Error!"+e.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
            }
        });

    }

    private void afterRegister(String userId, String nama, String email) {
        Log.d(TAG, "Now what to do after signing up?");
        UserAccount user = new UserAccount(nama, email);
        mDatabase.child("users").child(userId).child("username").setValue(user);
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void initializeUI() {
        //Hanya untuk inisialisasi layout
        coord = findViewById(R.id.coord_register);
        mNama = findViewById(R.id.edit_namalengkap);
        mEmail = findViewById(R.id.edit_email);
        mPassword1 = findViewById(R.id.edit_pass1);
        mPassword2 = findViewById(R.id.edit_pass2);
        progressBar = findViewById(R.id.progress_regist);
        registBtn = findViewById(R.id.btn_go_register);
        loginBtn = findViewById(R.id.btn_register_login);
    }
}

class UserAccount {
    //Inner class untuk login. Sebenarnya mungkin tidak dibutuhkan ,-,
    String nama, email;

    public UserAccount(String nama, String email) {
        this.nama = nama;
        this.email = email;
    }
}